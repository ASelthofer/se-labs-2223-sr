from query_handler_base import QueryHandlerBase
import random
import requests
import json

class GeniousHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "lyrics" in query:
            return True
        return False

    def process(self, query):
        names = query.split()
        term = names[1] + names[2]

        try:
            result = self.call_api(term)
            self.ui.say("Hope you found what you were looking for")
            self.ui.say(f"Your word was {term}")
        except: 
            self.ui.say("Oh no! There was an error trying to contact urban dictionary api.")
            self.ui.say("Try something else!")



    def call_api(self, term):
        url = 'https://genius-song-lyrics1.p.rapidapi.com/search'

        querystring = {"term":term}

        headers = {
	         "X-RapidAPI-Key": "743d8790famsha9fb71187a3f316p111c88jsnb11188a72369",
	         "X-RapidAPI-Host": "genius-song-lyrics1.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers, params=querystring)

        print(response.text)
