from query_handler_base import QueryHandlerBase
import random
import requests
import json

class DarkSkyHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "sky" in query:
            return True
        return False

    def process(self, query):
        names = query.split()
        latitude = names[1]
        longitude = names[2]

        try:
            result = self.call_api(latitude, longitude)
            self.ui.say("Hope you found what you were looking for")
            self.ui.say(f"Your word was {latitude, longitude}")
        except: 
            self.ui.say("Oh no! There was an error trying to contact urban dictionary api.")
            self.ui.say("Try something else!")



    def call_api(self, longitude, latitude):
        url = 'https://dark-sky.p.rapidapi.com/%7Blatitude%7D,%7Blongitude%7D'

        querystring = {"longitude":longitude, "latitude":latitude}

        headers = {
	'X-RapidAPI-Key': '743d8790famsha9fb71187a3f316p111c88jsnb11188a72369',
    'X-RapidAPI-Host': 'dark-sky.p.rapidapi.com'
}

        response = requests.request("GET", url, headers=headers, params=querystring)

        print(response.text)
