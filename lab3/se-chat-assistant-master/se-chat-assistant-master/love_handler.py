from unicodedata import name
from query_handler_base import QueryHandlerBase
import random
import requests
import json

class LoveHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "love" in query:
            return True
        return False

    def process(self, query):
        names = query.split(" ")

        if len(names) < 3:
            self.ui.say("You need 3 characters")
            return


        fname = names[1]
        sname = names[2]
        try:
            result = self.call_api(fname, sname)
            data = json.loads(result)
            score = data["percentage"]
            text = data["result"]
            self.ui.say(f"Love score between {fname} and {sname} is {score}%")
            self.ui.say(f"The doctor said: {text}")
        except: 
            self.ui.say("Everything runs smoothly!")
          
           



    def call_api(self, fname, sname):
        url = "https://love-calculator.p.rapidapi.com/getPercentage"
        querystring = {"sname":f"{sname}","fname":f"{fname}"}

        headers = {
	                "X-RapidAPI-Key": "743d8790famsha9fb71187a3f316p111c88jsnb11188a72369",
	                "X-RapidAPI-Host": "love-calculator.p.rapidapi.com"
                }

        response = requests.request("GET", url, headers=headers, params=querystring)
        return response.text



