#ZAD1
name = input("Give me your name: ")
age = int(input("How old are you: "))
year= 2022 - age + 100
print(name + ", you will be 100 years old in the year "+ str(year))

#ZAD2
num = input("Enter a number: ")
mod = num % 2
if mod > 0:
    print("You picked an odd number.")
else:
    print("You picked an even number.")

#ZAD3
a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
for x in a:
    if x <5: 
        print(x)

print( [ x for x in a if x<5 ] )

#ZAD4
num = int(input("Please choose a number to divide: "))
listRange = list(range(1,num+1))
divisorList = []
for number in listRange:
    if num % number == 0:
        divisorList.append(number)
print(divisorList)



  